package ru.nsu.ccfit.skokova;

public class Main {
    public static void main(String[] args) {
        try {
            String properties = "config.xml";
            String name = "Anon";

            if ((args.length == 2) && (args[0].equals("-name"))) {
                name = args[1];
            }

            Chat chat = new Chat(properties, name);
            chat.start();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
