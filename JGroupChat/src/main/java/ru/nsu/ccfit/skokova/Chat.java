package ru.nsu.ccfit.skokova;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.util.Util;

public class Chat {
    private JChannel jChannel;

    public Chat(String properties, String name) throws Exception {
        jChannel = new JChannel(properties).name(name);
    }

    public void start() throws Exception {
        jChannel.connect("ChatCluster");
        jChannel.setReceiver(new ReceiverAdapter() {
            public void receive(Message message) {
                System.out.println(">" + message.getSrc() + " : " + message.getObject());
            }

            @Override
            public void viewAccepted(View view) {
                super.viewAccepted(view);
                System.out.println("** view: " + view);
            }
        });
        eventLoop();
        jChannel.close();
    }

    private void eventLoop() throws Exception {
        while (true) {
            String messageString = Util.readLine(System.in);
            if (messageString.equalsIgnoreCase("exit")
                    || messageString.equalsIgnoreCase("quit")) {
                break;
            }
            Message message = new Message(null, messageString);
            jChannel.send(message);
        }
    }
}
