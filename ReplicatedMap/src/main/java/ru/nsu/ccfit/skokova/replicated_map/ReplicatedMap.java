package ru.nsu.ccfit.skokova.replicated_map;

import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.blocks.locking.LockService;
import org.jgroups.util.RspList;
import org.jgroups.util.Util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;

public class ReplicatedMap extends ReceiverAdapter {
    private static final int TIMEOUT = 30000;

    private final Map<String, Double> stocks = new HashMap<>();
    private final Lock lock;
    private RpcDispatcher dispatcher; // to invoke RPCs

    public ReplicatedMap(JChannel channel) throws Exception {
        this.lock = new LockService(channel).getLock("MAP_LOCK");

        this.dispatcher = new RpcDispatcher(channel, this);
        dispatcher.setMembershipListener(this);
        dispatcher.setStateListener(this);
        dispatcher.start();

        channel.getState(null, TIMEOUT);
    }

    /**
     * Assigns a value to a stock
     */
    public void _setStock(String name, double value) {
        synchronized (stocks) {
            stocks.put(name, value);
            System.out.printf("-- set %s to %s\n", name, value);
        }
    }

    /**
     * Removes a stock from the hashmap
     */
    public void _removeStock(String name) {
        synchronized (stocks) {
            stocks.remove(name);
            System.out.printf("-- removed %s\n", name);
        }
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        DataOutput out = new DataOutputStream(output);
        lock.lock();
        try {
            synchronized (stocks) {
                Util.objectToStream(stocks, out);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void setState(InputStream input) throws Exception {
        DataInput in = new DataInputStream(input);
        lock.lock();
        try {
            Map<String, Double> newState = Util.objectFromStream(in);
            System.out.println("-- received state: " + newState.size() + " stocks");
            synchronized (stocks) {
                stocks.clear();
                stocks.putAll(newState);
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean remove(String key) {
        try {
            lock.lock();
            RspList<Boolean> responses = dispatcher.callRemoteMethods(
                    null,
                    "_removeStock",
                    new Object[]{key},
                    new Class[]{String.class},
                    RequestOptions.SYNC()
            );
            return responses.getSuspectedMembers().isEmpty()
                    && responses.getResults().stream().allMatch(Boolean::booleanValue);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    public boolean put(String key, double value) {
        try {
            lock.lock();
            RspList<Boolean> responses = dispatcher.callRemoteMethods(
                    null,
                    "_setStock",
                    new Object[]{key, value},
                    new Class[]{String.class, double.class},
                    RequestOptions.SYNC()
            );
            return responses.getSuspectedMembers().isEmpty()
                    && responses.getResults().stream().allMatch(Boolean::booleanValue);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

    public Double get(String key) {
        Double value;
        lock.lock();
        try {
            synchronized (stocks) {
                value = stocks.get(key);
            }
        } finally {
            lock.unlock();
        }
        return value;
    }

    public Set<Map.Entry<String, Double>> entrySet() {
        Set<Map.Entry<String, Double>> entrySet;
        synchronized (stocks) {
            entrySet = stocks.entrySet();
        }
        return entrySet;
    }

    public boolean compareAndSwap(String key, double oldValue, double newValue) {
        Double currentValue;
        lock.lock();
        try {
            currentValue = stocks.get(key);
            if (Objects.equals(currentValue, oldValue)) {
                RspList<Boolean> responses = dispatcher.callRemoteMethods(
                        null,
                        "_setStock",
                        new Object[]{key, newValue},
                        new Class[]{String.class, double.class},
                        RequestOptions.SYNC()
                );
                return responses.getSuspectedMembers().isEmpty()
                        && responses.getResults().stream().allMatch(Boolean::booleanValue);
            }
            return false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }
}
