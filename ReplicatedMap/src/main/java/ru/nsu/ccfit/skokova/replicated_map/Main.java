package ru.nsu.ccfit.skokova.replicated_map;

import org.jgroups.JChannel;
import org.jgroups.util.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private static final String SUCCESS = "Success";
    private static final String ERROR = "Error";
    private JChannel channel;

    private final ReplicatedMap replicatedMap;

    private Map<Character, Command> commandMap;

    private Scanner scanner;

    {
        commandMap = new HashMap<>();
        commandMap.put('1', this::showStocks);
        commandMap.put('2', this::getQuote);
        commandMap.put('3', this::setQuote);
        commandMap.put('4', this::removeQuote);
        commandMap.put('5', this::compareAndSwapQuote);
        commandMap.put('x', this::exit);
    }

    public Main(String configFile) throws Exception {
        scanner = new Scanner(System.in);

        this.channel = new JChannel(configFile);
        channel.connect("Stocks");
        this.replicatedMap = new ReplicatedMap(channel);
    }

    private void runEventLoop() {
        while (true) {
            char pressedKey = (char) Util.keyPress("");
            if (commandMap.containsKey(pressedKey)) {
                commandMap.get(pressedKey).execute();
            }
        }
    }

    public static void main(String[] args) {
        try {
            Main main = new Main("jgroups-config.xml");
            main.runEventLoop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showStocks() {
        synchronized (replicatedMap) {
            for (Map.Entry<String, Double> entry : replicatedMap.entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }
        }
    }

    private void getQuote() {
        String key = readInput("key");
        Double value = replicatedMap.get(key);
        if (value != null) {
            System.out.println("Value: " + value);
        } else {
            System.out.println("There is no value for this key");
        }
    }

    private void setQuote() {
        String key = readInput("key");
        String valueString = readInput("value");
        try {
            double value = Double.parseDouble(valueString);
            boolean isSet = replicatedMap.put(key, value);
            String message = isSet ? SUCCESS : ERROR;
            System.out.println(message);
        } catch (NumberFormatException e) {
            System.err.println("Incorrect value: " + valueString);
        }
    }

    private void removeQuote() {
        String key = readInput("key");
        boolean isRemoved = replicatedMap.remove(key);
        String message = isRemoved ? SUCCESS : ERROR;
        System.out.println(message);
    }

    private void compareAndSwapQuote() {
        String key = readInput("key");
        String oldValueString = readInput("old value");
        String newValueString = readInput("new value");
        try {
            double oldValue = Double.parseDouble(oldValueString);
            double newValue = Double.parseDouble(newValueString);
            boolean isSet = replicatedMap.compareAndSwap(key, oldValue, newValue);
            String message = isSet ? SUCCESS : ERROR;
            System.out.println(message);
        } catch (NumberFormatException e) {
            System.err.println("Incorrect value");
        }
    }

    private void exit() {
        channel.close();
        System.exit(0);
    }

    private String readInput(String valueName) {
        System.out.println("Enter the " + valueName + ": ");
        return scanner.nextLine();
    }

    private interface Command {
        void execute();
    }
}
